FROM ubuntu:20.04

LABEL maintainer="stefan@fluit-online.nl"

# Refresh apt cache
RUN apt-get update -y

# Install software-properties
RUN apt-get install -y software-properties-common

# Install pip
RUN apt-get install -y python3-pip

# Upgrade pip
RUN pip install --upgrade pip

# Install Ansible ppa
RUN apt-add-repository ppa:ansible/ansible -y

# Refresh apt cache
RUN apt-get update -y

# Install Ansible
RUN apt-get install ansible -y

# Ensure all files are accessible
COPY . /app

# Install Ansible roles
RUN ansible-galaxy install -r /app/requirements.yml --ignore-errors

# Install pip packages
RUN pip install -r /app/requirements.txt