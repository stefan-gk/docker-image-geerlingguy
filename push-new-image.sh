#!/usr/bin/env bash

set -e errexit

declare DOCKER_IMAGE_NAME="stefanfluit/docker-image-geerlingguy"
declare DOCKER_IMAGE_TAG="latest"

# Build the new image
docker build -t "${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}" .

# Push the new image
docker push "${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}"