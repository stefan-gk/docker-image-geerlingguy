# Docker container with all Geerlingguy roles installed #

Docker container with all Geerlingguy roles installed, as well as awscli, boto3 and botocore.  
Other roles are in `requirements.yml`.

### Docker hub page ###
[https://hub.docker.com/repository/docker/stefanfluit/docker-image-geerlingguy](https://hub.docker.com/repository/docker/stefanfluit/docker-image-geerlingguy)

### Bitbucket repository ###
[https://bitbucket.org/stefan-gk/docker-image-geerlingguy/src/main/](https://bitbucket.org/stefan-gk/docker-image-geerlingguy/src/main/)

### What is installed? ###

* Latest Ansible version via Ansible apt repository
* All required Python packages from `requirements.txt`
* All roles I use from `requirements.yml`

### How do I get set up? ###

* `docker pull stefanfluit/docker-image-geerlingguy`

### Tags ###

* Only latest for now

### Who do I talk to? ###

* stefan@fluit-online.nl

### Note: ###

* You should not use this in production. I wrote it to test some playbooks with a lot of Geerlingguy roles.

### Example bitbucket-pipelines.yml file for Bitbucket automation ###

```
image: stefanfluit/docker-image-geerlingguy:latest

pipelines:
  branches:
    main:
      - step:
          name: Validate Ansible files before running the pipeline
          script:
            - ansible-playbook ansible-playbook.yml -i inventory.yml --syntax-check

      - step:
          name: Run playbook
          script:
            - ansible-playbook ansible-playbook.yml -i inventory.yml
```
